module cep-elibrary

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.17.0
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/rs/zerolog v1.28.0
	github.com/spf13/viper v1.12.0
	golang.org/x/crypto v0.0.0-20220817201139-bc19a97f63c8
	golang.org/x/sys v0.0.0-20220909162455-aba9fc2a8ff2 // indirect
	google.golang.org/api v0.94.0
)

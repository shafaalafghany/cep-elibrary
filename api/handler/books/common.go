package books

import "fmt"

func ValidateKeyword(keyword string) error {
	if keyword == "" {
		return fmt.Errorf("keyword cannot be empty")
	}
	return nil
}

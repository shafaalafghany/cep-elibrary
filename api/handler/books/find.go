package books

import (
	"cep-elibrary/store"
	"fmt"
	"net/http"

	"cep-elibrary/api/common"
	apierror "cep-elibrary/api/error"
	"cep-elibrary/api/response"

	"github.com/rs/zerolog"
)

type FindRequest struct {
	Keyword string `json:"keyword"`
}

type FindResponse struct {
	Data []*store.Book `json:"data"`
}

func (fr *FindRequest) validateFindRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateKeyword(fr.Keyword); err != nil {
		field := apierror.InvalidField{
			Name:    "keyword",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func FindByAuthorAndTitle(
	zlog zerolog.Logger,
	bookStore store.BookStore,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := FindRequest{}
		req.Keyword = r.URL.Query().Get("keyword")
		if fieldErr := req.validateFindRequest(); fieldErr != nil {
			response.ValidationError(w, *fieldErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		book, err := bookStore.FindByAuthorAndTitle(ctx, req.Keyword, req.Keyword)
		if err != nil {
			err = fmt.Errorf("bookStore.FindByAuthorAndTitle: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find by author and title")
			response.Error(w, apierror.ServerError())
			return
		}
		res := FindResponse{
			Data: book,
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

func FindNewBooks(
	zlog zerolog.Logger,
	bookStore store.BookStore,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		book, err := bookStore.FindNewBooks(ctx)
		if err != nil {
			err = fmt.Errorf("bookStore.FindNewBooks: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find new books")
			response.Error(w, apierror.ServerError())
			return
		}
		res := FindResponse{
			Data: book,
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

func FindPopularBooks(
	zlog zerolog.Logger,
	bookStore store.BookStore,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		book, err := bookStore.FindPopularBooks(ctx)
		if err != nil {
			err = fmt.Errorf("bookStore.FindPopularBooks: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find popular books")
			response.Error(w, apierror.ServerError())
			return
		}
		res := FindResponse{
			Data: book,
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

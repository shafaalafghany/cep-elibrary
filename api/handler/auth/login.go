package auth

import (
	"cep-elibrary/api/common"
	"cep-elibrary/api/response"
	"cep-elibrary/googleauth"
	"cep-elibrary/store"
	"cep-elibrary/token"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	apierror "cep-elibrary/api/error"

	"github.com/rs/zerolog"
	"golang.org/x/crypto/bcrypt"
)

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginOauthRequest struct {
	Token string `json:"token"`
	Email string `json:"email"`
}

type Token struct {
	TokenName string    `json:"token_name"`
	TokenType string    `json:"token_type"`
	Token     string    `json:"token"`
	ExpireAt  time.Time `json:"expire_at"`
	Scheme    string    `json:"scheme"`
}

type LoginResponse struct {
	Tokens []Token `json:"token"`
}

func (lr *LoginRequest) validateLoginRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateEmail(lr.Email); err != nil {
		field := apierror.InvalidField{
			Name:    "email",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if lr.Password == "" {
		field := apierror.InvalidField{
			Name:    "password",
			Message: "password cannot be empty",
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

const (
	accessTokenName  = "access_token"
	accessTokenType  = "access"
	refreshTokenName = "refresh_token"
	refreshTokenType = "refresh"
)

func Login(
	zlog zerolog.Logger,
	userStore store.UserStore,
	jwt token.JWT,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := LoginRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldsErr := req.validateLoginRequest(); fieldsErr != nil {
			response.ValidationError(w, *fieldsErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		usr, err := userStore.FindOneCredentialByEmail(ctx, req.Email)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				response.Error(w, apierror.ClientUnauthorized())
				return
			}
			err = fmt.Errorf("userStore.FindOneCredentialByEmail: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find one credential by email")
			response.Error(w, apierror.ServerError())
			return
		} else {
			if !usr.IsVerified {
				response.Error(w, apierror.ClientInactiveUser())
				return
			}
		}
		err = bcrypt.CompareHashAndPassword([]byte(usr.Password.String), []byte(req.Password))
		if err != nil {
			response.Error(w, apierror.ClientInvalidCredential())
			return
		}
		claim := token.Claim{
			UserId: usr.ID,
		}
		tokenId := make([]byte, 12)
		_, err = rand.Read(tokenId)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to generate tokenId")
			response.Error(w, apierror.ServerError())
			return
		}
		jti := base64.RawStdEncoding.EncodeToString(tokenId)
		err = userStore.UpdateTokenIdById(ctx, jti, usr.ID)
		if err != nil {
			err = fmt.Errorf("userStore.UpdateTokenIdById: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to update token_id by id")
			response.Error(w, apierror.ServerError())
			return
		}
		accesstoken, err := jwt.CreateAccessToken(claim)
		if err != nil {
			err = fmt.Errorf("jwt.CreateAccessToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msgf("failed to generate access token")
			response.Error(w, apierror.ServerError())
			return
		}
		claim.TokenId = jti
		refreshToken, err := jwt.CreateRefreshToken(claim)
		if err != nil {
			err = fmt.Errorf("jwt.CreateRefreshToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msgf("failed to generate refresh token")
			response.Error(w, apierror.ServerError())
			return
		}
		token := []Token{
			{
				TokenName: accessTokenName,
				TokenType: accessTokenType,
				Token:     accesstoken.Token,
				ExpireAt:  accesstoken.ExpireAt,
				Scheme:    accesstoken.Scheme,
			},
			{
				TokenName: refreshTokenName,
				TokenType: refreshTokenType,
				Token:     refreshToken.Token,
				ExpireAt:  refreshToken.ExpireAt,
				Scheme:    refreshToken.Scheme,
			},
		}
		res := LoginResponse{
			Tokens: token,
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

func (lr *LoginOauthRequest) validateLoginRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateEmail(lr.Email); err != nil {
		field := apierror.InvalidField{
			Name:    "email",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if lr.Token == "" {
		field := apierror.InvalidField{
			Name:    "token",
			Message: "token cannot be empty",
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func LoginGoogle(
	zlog zerolog.Logger,
	userStore store.UserStore,
	jwt token.JWT,
	googleAuthValidator googleauth.Validator,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := LoginOauthRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldsErr := req.validateLoginRequest(); fieldsErr != nil {
			response.ValidationError(w, *fieldsErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		googleClaim, err := googleAuthValidator.Validate(ctx, req.Token)
		if err != nil {
			wlog.Warn(ctx).
				Err(err).Msg("failed to authorize google token")
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		if req.Email != googleClaim.Email {
			err = fmt.Errorf("email does not match token owner")
			wlog.Warn(ctx).
				Err(err).Msg("email does not match token owner")
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		usr, err := userStore.FindOneByEmail(ctx, googleClaim.Email)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				response.Error(w, apierror.ClientUnauthorized())
				return
			}
			err = fmt.Errorf("userStore.FindOneByEmail: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find one by email")
			response.Error(w, apierror.ClientNotFound())
			return
		} else {
			if !usr.IsVerified {
				response.Error(w, apierror.ClientInactiveUser())
				return
			}
		}
		claim := token.Claim{
			UserId: usr.ID,
		}
		tokenId := make([]byte, 12)
		_, err = rand.Read(tokenId)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to generate tokenId")
			response.Error(w, apierror.ServerError())
			return
		}
		jti := base64.RawStdEncoding.EncodeToString(tokenId)
		err = userStore.UpdateTokenIdById(ctx, jti, usr.ID)
		if err != nil {
			err = fmt.Errorf("userStore.UpdateTokenIdById: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to update token_id by id")
			response.Error(w, apierror.ServerError())
			return
		}
		accesstoken, err := jwt.CreateAccessToken(claim)
		if err != nil {
			err = fmt.Errorf("jwt.CreateAccessToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msgf("failed to generate access token")
			response.Error(w, apierror.ServerError())
			return
		}
		claim.TokenId = jti
		refreshToken, err := jwt.CreateRefreshToken(claim)
		if err != nil {
			err = fmt.Errorf("jwt.CreateRefreshToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msgf("failed to generate refresh token")
			response.Error(w, apierror.ServerError())
			return
		}
		token := []Token{
			{
				TokenName: accessTokenName,
				TokenType: accessTokenType,
				Token:     accesstoken.Token,
				ExpireAt:  accesstoken.ExpireAt,
				Scheme:    accesstoken.Scheme,
			},
			{
				TokenName: refreshTokenName,
				TokenType: refreshTokenType,
				Token:     refreshToken.Token,
				ExpireAt:  refreshToken.ExpireAt,
				Scheme:    refreshToken.Scheme,
			},
		}
		res := LoginResponse{
			Tokens: token,
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

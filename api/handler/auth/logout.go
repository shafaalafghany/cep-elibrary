package auth

import (
	"cep-elibrary/api/common"
	apierror "cep-elibrary/api/error"
	"cep-elibrary/api/response"
	"cep-elibrary/store"
	"cep-elibrary/token"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rs/zerolog"
)

type LogoutRequest struct {
	Token string `json:"token"`
}

type LogoutResponse struct {
	Message string `json:"message"`
}

func (lr *LogoutRequest) validateLogoutRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateToken(lr.Token); err != nil {
		field := apierror.InvalidField{
			Name:    "token",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func Logout(
	zlog zerolog.Logger,
	userStore store.UserStore,
	jwt token.JWT,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := LogoutRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldErr := req.validateLogoutRequest(); fieldErr != nil {
			response.ValidationError(w, *fieldErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		claim, err := jwt.ExpectRefreshToken(req.Token)
		if err != nil {
			err = fmt.Errorf("jwt.ExpectRefreshToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msgf("failed to expect refresh token")
			response.Error(w, apierror.ServerError())
			return
		}
		usr, err := userStore.FindOneById(ctx, claim.UserId)
		if err != nil {
			err = fmt.Errorf("userStore.FindOneById: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find one by id")
			response.Error(w, apierror.ServerError())
			return
		}
		if usr.TokenID.String != claim.TokenId {
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		err = userStore.DeleteTokenId(ctx, claim.UserId)
		if err != nil {
			err = fmt.Errorf("userStore.DeleteTokenId: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to delete token_id by id")
			response.Error(w, apierror.ServerError())
			return
		}
		res := LogoutResponse{
			Message: "account has been logout",
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

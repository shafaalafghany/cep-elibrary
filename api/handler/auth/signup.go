package auth

import (
	"cep-elibrary/api/common"
	apierror "cep-elibrary/api/error"
	"cep-elibrary/api/response"
	"cep-elibrary/googleauth"
	mailer "cep-elibrary/mail"
	"cep-elibrary/store"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/rs/zerolog"
	"golang.org/x/crypto/bcrypt"
)

type SignUpRequest struct {
	Fullname          string `json:"fullname"`
	Email             string `json:"email"`
	Password          string `json:"password"`
	IsVerified        bool   `json:"is_verified"`
	TokenVerification string `json:"token_verification"`
	TokenExpiration   string `json:"token_expiration"`
}

type SignUpOauthRequest struct {
	Email string `json:"email"`
	Token string `json:"token"`
}

type ActivateEmailRequest struct {
	ID    string
	Token string
}

type ResendEmailRequest struct {
	Email string `json:"email"`
}

type SignUpResponse struct {
	Message string `json:"message"`
}

type EmailActivationResponse struct {
	Message string `json:"message"`
}

type ResendEmailResponse struct {
	Message string `json:"message"`
}

const accountStatus = false

func (sr *SignUpRequest) validateSignupRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateEmail(sr.Email); err != nil {
		field := apierror.InvalidField{
			Name:    "email",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if err = ValidatePassword(sr.Password); err != nil {
		field := apierror.InvalidField{
			Name:    "password",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if err = ValidateName(sr.Fullname); err != nil {
		field := apierror.InvalidField{
			Name:    "fullname",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func Signup(
	zlog zerolog.Logger,
	userStore store.UserStore,
	mail mailer.EmailSender,
	tokenExpiration time.Duration,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := SignUpRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldsErr := req.validateSignupRequest(); fieldsErr != nil {
			response.ValidationError(w, *fieldsErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		_, err := userStore.FindOneByEmail(ctx, req.Email)
		if err == nil {
			response.Error(w, apierror.ClientAlreadyExists())
			return
		}
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to generate bcrypt")
			response.Error(w, apierror.ServerError())
			return
		}
		req.Password = string(hashedPassword)
		req.IsVerified = accountStatus
		req.TokenVerification, err = RandString(128)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to generate random string")
			response.Error(w, apierror.ServerError())
			return
		}
		req.TokenExpiration = strconv.Itoa(int(time.Now().Add(tokenExpiration * time.Minute).Unix()))
		err = registerNewUser(ctx, userStore, req)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to insert new user")
			response.Error(w, apierror.ServerError())
			return
		}
		user, err := userStore.FindOneByEmail(ctx, req.Email)
		if err != nil {
			err = fmt.Errorf("userStore.FindOneByEmail: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find one by email")
			response.Error(w, apierror.ServerError())
			return
		}
		mail.SendActivationLink(user.ID, req.Email, user.TokenVerification.String)
		res := SignUpResponse{
			Message: "email activation has been sent, please check your email",
		}
		response.GenerateResponse(w, http.StatusCreated, res)
	}
}

func (sr *SignUpOauthRequest) validateSignupRequest() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateEmail(sr.Email); err != nil {
		field := apierror.InvalidField{
			Name:    "email",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if err = ValidateTokenGoogle(sr.Token); err != nil {
		field := apierror.InvalidField{
			Name:    "token",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func SignupGoogle(
	zlog zerolog.Logger,
	userStore store.UserStore,
	googleAuthValidator googleauth.Validator,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := SignUpOauthRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldsErr := req.validateSignupRequest(); fieldsErr != nil {
			response.ValidationError(w, *fieldsErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		googleClaim, err := googleAuthValidator.Validate(ctx, req.Token)
		if err != nil {
			wlog.Warn(ctx).
				Err(err).Msg("failed to authorize google token")
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		if req.Email != googleClaim.Email {
			err = fmt.Errorf("email does not match token owner")
			wlog.Warn(ctx).
				Err(err).Msg("email does not match token owner")
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		_, err = userStore.FindOneByEmail(ctx, req.Email)
		if err == nil {
			response.Error(w, apierror.ClientAlreadyExists())
			return
		}
		usr := store.User{
			Email:      googleClaim.Email,
			Fullname:   googleClaim.Fullname,
			IsVerified: googleClaim.IsVerified,
		}
		err = userStore.InsertByGoogle(ctx, &usr)
		if err != nil {
			err = fmt.Errorf("userStore.InsertByGoogle: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to insert new user using google account")
			response.Error(w, apierror.ServerError())
			return
		}
		res := SignUpResponse{
			Message: "new account created",
		}
		response.GenerateResponse(w, http.StatusCreated, res)
	}
}

func ActivateEmail(
	zlog zerolog.Logger,
	userStore store.UserStore,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := ActivateEmailRequest{}
		req.ID = r.URL.Query().Get("id")
		req.Token = r.URL.Query().Get("token")
		if fieldErr := req.validateActivateEmail(); fieldErr != nil {
			response.ValidationError(w, *fieldErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		id, err := strconv.Atoi(req.ID)
		if err != nil {
			response.Error(w, apierror.ClientInvalidCredential())
			return
		}
		usr, err := userStore.FindOneById(ctx, id)
		if err != nil {
			response.Error(w, apierror.ClientNotFound())
			return
		}
		if req.Token != usr.TokenVerification.String {
			response.Error(w, apierror.ClientInvalidToken())
			return
		}
		tokenExpiration, _ := strconv.Atoi(usr.TokenExpiration.String)
		if int64(tokenExpiration) < time.Now().Unix() {
			response.Error(w, apierror.ClientAccessExpired())
			return
		}
		err = userStore.VerifyUserByToken(ctx, id)
		if err != nil {
			err = fmt.Errorf("userStore.VerifyUserByToken: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to verify token user")
			response.Error(w, apierror.ServerError())
			return
		}
		res := EmailActivationResponse{
			Message: "email activated successfully",
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

func (ac *ActivateEmailRequest) validateActivateEmail() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateID(ac.ID); err != nil {
		field := apierror.InvalidField{
			Name:    "id",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	if err = ValidateToken(ac.Token); err != nil {
		field := apierror.InvalidField{
			Name:    "token",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func (re *ResendEmailRequest) validateResendEmail() *apierror.UnprocessableEntity {
	var err error
	if err = ValidateEmail(re.Email); err != nil {
		field := apierror.InvalidField{
			Name:    "email",
			Message: err.Error(),
		}
		fieldErr := apierror.ClientInvalidField(field)
		return &fieldErr
	}
	return nil
}

func ResendEmail(
	zlog zerolog.Logger,
	userStore store.UserStore,
	mail mailer.EmailSender,
	tokenExpiration time.Duration,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := ResendEmailRequest{}
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			response.Error(w, apierror.ClientBadRequest())
			return
		}
		if fieldsErr := req.validateResendEmail(); fieldsErr != nil {
			response.ValidationError(w, *fieldsErr)
			return
		}
		ctx := r.Context()
		wlog := common.WrapperZlog{Logger: &zlog}
		usr, err := userStore.FindOneByEmail(ctx, req.Email)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				response.Error(w, apierror.ClientNotFound())
				return
			}
			err = fmt.Errorf("userStore.FindOneByEmail: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to find one by email")
			response.Error(w, apierror.ServerError())
			return
		}
		tokenVerificationExpiration, _ := strconv.Atoi(usr.TokenExpiration.String)
		if time.Now().Unix() < int64(tokenVerificationExpiration) {
			response.Error(w, apierror.ClientUnauthorized())
			return
		}
		tokenVerification, err := RandString(128)
		if err != nil {
			wlog.Error(ctx).
				Err(err).Msg("failed to generate random string")
			response.Error(w, apierror.ServerError())
			return
		}
		tokenExp := strconv.Itoa(int(time.Now().Add(tokenExpiration * time.Minute).Unix()))
		data := &store.UserRegister{
			Email:             usr.Email,
			TokenVerification: tokenVerification,
			TokenExpiration:   tokenExp,
		}
		err = userStore.UpdateTokenByEmail(ctx, data)
		if err != nil {
			err = fmt.Errorf("userStore.UpdateTokenByEmail: %w", err)
			wlog.Error(ctx).
				Err(err).Msg("failed to update token by email")
			response.Error(w, apierror.ServerError())
			return
		}
		mail.SendActivationLink(usr.ID, req.Email, tokenVerification)
		res := ResendEmailResponse{
			Message: "resend email successfully",
		}
		response.GenerateResponse(w, http.StatusOK, res)
	}
}

func registerNewUser(
	ctx context.Context,
	userStore store.UserStore,
	user SignUpRequest,
) error {
	usr := &store.UserRegister{
		Email:             user.Email,
		Password:          user.Password,
		Fullname:          user.Fullname,
		IsVerified:        user.IsVerified,
		TokenVerification: user.TokenVerification,
		TokenExpiration:   user.TokenExpiration,
	}
	err := userStore.Insert(ctx, usr)
	if err != nil {
		return fmt.Errorf("userStore.Insert: %w", err)
	}
	return nil
}

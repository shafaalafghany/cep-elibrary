package api

import (
	"cep-elibrary/api/handler/auth"
	"cep-elibrary/api/handler/books"
	"cep-elibrary/googleauth"
	mailer "cep-elibrary/mail"
	"cep-elibrary/store"
	"cep-elibrary/store/postgresql"
	"cep-elibrary/token"
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog"
)

type Server struct {
	Addr                string
	logger              zerolog.Logger
	stores              *stores
	googleAuthValidator googleauth.Validator
	jwt                 token.JWT
	mail                mailer.EmailSender
	tokenVerification   TokenVerificationConfig
}

type DB struct {
	ELibraryPostgres *sql.DB
}

type stores struct {
	userStore store.UserStore
	bookStore store.BookStore
}

type TokenVerificationConfig struct {
	Expiry time.Duration
}

func NewServer(
	addr string,
	logger zerolog.Logger,
	db DB,
	jwt token.JWT,
	googleAuthValidator googleauth.Validator,
	mail mailer.EmailSender,
	tokenVerification TokenVerificationConfig,
) *Server {
	s := &Server{
		Addr:                addr,
		logger:              logger,
		jwt:                 jwt,
		googleAuthValidator: googleAuthValidator,
		mail:                mail,
		tokenVerification:   tokenVerification,
	}
	var err error
	s.stores, err = initStores(s, db)
	if err != nil {
		logger.Fatal().Err(err)
	}
	return s
}

func initStores(s *Server, db DB) (*stores, error) {
	stores := &stores{}
	var err error
	if stores.userStore, err = postgresql.NewUserStore(
		s.logger.With().Str("store", "user_store").Logger(),
		db.ELibraryPostgres,
	); err != nil {
		return nil, err
	}
	if stores.bookStore, err = postgresql.NewBookStore(db.ELibraryPostgres); err != nil {
		return nil, err
	}
	return stores, nil
}

func (s *Server) Run(ctx context.Context) {
	handler := chi.NewMux()
	handler.Mount("/", handlers(s))

	srv := &http.Server{
		Addr:    s.Addr,
		Handler: handler,
	}
	s.logger.Info().Msgf("serving http on: %s\n", s.Addr)
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		s.logger.Error().Err(fmt.Errorf("failed to serve http: %w", err))
	}
}

func handlers(s *Server) http.Handler {
	h := chi.NewMux()
	validateToken := token.Validate(s.jwt)

	h.Post("/auth", auth.Signup(
		s.logger,
		s.stores.userStore,
		s.mail,
		s.tokenVerification.Expiry,
	))
	h.Post("/auth/login", auth.Login(
		s.logger,
		s.stores.userStore,
		s.jwt,
	))
	h.Post("/auth/logout", auth.Logout(
		s.logger,
		s.stores.userStore,
		s.jwt,
	))
	h.Post("/auth/google", auth.SignupGoogle(
		s.logger,
		s.stores.userStore,
		s.googleAuthValidator,
	))
	h.Post("/auth/google/login", auth.LoginGoogle(
		s.logger,
		s.stores.userStore,
		s.jwt,
		s.googleAuthValidator,
	))
	h.Get("/auth/verify", auth.ActivateEmail(
		s.logger,
		s.stores.userStore,
	))
	h.Post("/auth/resend", auth.ResendEmail(
		s.logger,
		s.stores.userStore,
		s.mail,
		s.tokenVerification.Expiry,
	))
	h.With(validateToken).Get("/books", books.FindByAuthorAndTitle(
		s.logger,
		s.stores.bookStore,
	))
	h.With(validateToken).Get("/books/new", books.FindNewBooks(
		s.logger,
		s.stores.bookStore,
	))
	h.With(validateToken).Get("/books/popular", books.FindPopularBooks(
		s.logger,
		s.stores.bookStore,
	))
	return h
}

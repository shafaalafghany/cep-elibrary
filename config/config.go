package config

import "github.com/spf13/viper"

type Config struct {
	AppHost                     string `mapstructure:"APP_HOST"`
	AppPort                     int    `mapstructure:"APP_PORT"`
	AppProtocol                 string `mapstructure:"APP_PROTOCOL"`
	DbHost                      string `mapstructure:"DB_HOST"`
	DbPort                      int    `mapstructure:"DB_PORT"`
	DbName                      string `mapstructure:"DB_NAME"`
	DbUsername                  string `mapstructure:"DB_USERNAME"`
	DbPassword                  string `mapstructure:"DB_PASSWORD"`
	GoogleOauthAudience         string `mapstructure:"GOOGLE_OAUTH_AUDIENCE"`
	LoggerLevel                 string `mapstructure:"LOGGER_LEVEL"`
	LoggerOutput                string `mapstructure:"LOGGER_OUTPUT"`
	MailHost                    string `mapstructure:"MAIL_HOST"`
	MailPort                    int    `mapstructure:"MAIL_PORT"`
	MailUsername                string `mapstructure:"MAIL_USERNAME"`
	MailPassword                string `mapstructure:"MAIL_PASSWORD"`
	TokenAccessExpiration       int    `mapstructure:"TOKEN_ACCESS_EXPIRATION_MINUTE"`
	TokenRefreshExpiration      int    `mapstructure:"TOKEN_REFRESH_EXPIRATION_MINUTE"`
	TokenVerificationExpiration int    `mapstructure:"TOKEN_VERIFICATION_EXPIRATION_MINUTE"`
}

func LoadConfig(path string) (Config, error) {
	config := Config{}
	viper.AddConfigPath(path)
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return config, err
	}

	err = viper.Unmarshal(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}

package postgresql

import (
	"cep-elibrary/store"
	"context"
	"database/sql"
	"fmt"

	"github.com/rs/zerolog"
)

type UserStore struct {
	log zerolog.Logger
	db  *sql.DB
	ps  *userPrepareStatement
}

func NewUserStore(log zerolog.Logger, db *sql.DB) (*UserStore, error) {
	us := &UserStore{
		db:  db,
		log: log,
		ps:  &userPrepareStatement{},
	}
	err := us.prepareStatement()
	if err != nil {
		return nil, err
	}
	return us, nil
}

type userPrepareStatement struct {
	DeleteTokenById          *sql.Stmt
	FindOneByEmail           *sql.Stmt
	FindOneById              *sql.Stmt
	FindOneCredentialByEmail *sql.Stmt
	Insert                   *sql.Stmt
	InsertByGoogle           *sql.Stmt
	UpdateTokenByEmail       *sql.Stmt
	UpdateTokenIdById        *sql.Stmt
	VerifyUserByToken        *sql.Stmt
}

func (us *UserStore) prepareStatement() error {
	storeName := "UserStore"
	var err error
	if us.ps.DeleteTokenById, err = prepareStatement(us.db, storeName, "DeleteTokenById", userDeleteTokenById); err != nil {
		return err
	}
	if us.ps.FindOneByEmail, err = prepareStatement(us.db, storeName, "FindOneByEmail", userFindOneByEmail); err != nil {
		return err
	}
	if us.ps.FindOneById, err = prepareStatement(us.db, storeName, "FindOneById", userFindOneById); err != nil {
		return err
	}
	if us.ps.FindOneCredentialByEmail, err = prepareStatement(us.db, storeName, "FindOneCredentialByEmail", userFindOneCredentialByEmail); err != nil {
		return err
	}
	if us.ps.InsertByGoogle, err = prepareStatement(us.db, storeName, "InsertByGoogle", userInsertByGoogle); err != nil {
		return err
	}
	if us.ps.Insert, err = prepareStatement(us.db, storeName, "Insert", userInsert); err != nil {
		return err
	}
	if us.ps.UpdateTokenByEmail, err = prepareStatement(us.db, storeName, "UpdateTokenByEmail", userUpdateTokenByEmail); err != nil {
		return err
	}
	if us.ps.UpdateTokenIdById, err = prepareStatement(us.db, storeName, "userDeleteTokenById", userUpdateTokenId); err != nil {
		return err
	}
	if us.ps.VerifyUserByToken, err = prepareStatement(us.db, storeName, "VerifyUserByToken", verifyUserByToken); err != nil {
		return err
	}
	return nil
}

const userFindOneBase = `
SELECT id, email, fullname, is_verified,
token_id, token_verification, token_expiration
FROM "users"
`

const userFindOneByEmail = userFindOneBase + "WHERE email = $1"

func (us *UserStore) FindOneByEmail(ctx context.Context, email string) (*store.User, error) {
	row := us.ps.FindOneByEmail.QueryRowContext(ctx, email)
	return us.scanRow(row)
}

const userFindOneById = userFindOneBase + "WHERE id = $1"

func (us *UserStore) FindOneById(ctx context.Context, id int) (*store.User, error) {
	row := us.ps.FindOneById.QueryRowContext(ctx, id)
	return us.scanRow(row)
}

const userFindOneCredentialByEmail = `
SELECT id, email, password, is_verified
FROM "users"
WHERE email = $1
`

func (us *UserStore) FindOneCredentialByEmail(ctx context.Context, email string) (*store.User, error) {
	row := us.ps.FindOneCredentialByEmail.QueryRowContext(ctx, email)
	user := &store.User{}
	err := row.Scan(
		&user.ID, &user.Email, &user.Password,
		&user.IsVerified,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to scan: %w", err)
	}
	return user, nil
}

const userInsertByGoogle = `
INSERT INTO "users" (
	email, fullname, is_verified
) VALUES (
	$1, $2, $3
) RETURNING id
`

func (us *UserStore) InsertByGoogle(ctx context.Context, usr *store.User) error {
	_, err := us.ps.InsertByGoogle.ExecContext(ctx,
		usr.Email, usr.Fullname, usr.IsVerified,
	)
	if err != nil {
		return fmt.Errorf("failed to insert: %w", err)
	}
	return nil
}

const userInsert = `
INSERT INTO "users" (
	email, password, fullname, is_verified,
	token_verification, token_expiration
) VALUES (
	$1, $2, $3, $4, $5, $6
)
`

func (us *UserStore) Insert(ctx context.Context, usr *store.UserRegister) error {
	_, err := us.ps.Insert.ExecContext(ctx,
		usr.Email, usr.Password, usr.Fullname,
		usr.IsVerified, usr.TokenVerification,
		usr.TokenExpiration,
	)
	if err != nil {
		return fmt.Errorf("failed to Insert: %w", err)
	}
	return nil
}

const userUpdateTokenId = `
UPDATE "users" SET
token_id = $1
WHERE id = $2
`

func (us *UserStore) UpdateTokenIdById(ctx context.Context, token string, id int) error {
	_, err := us.ps.UpdateTokenIdById.ExecContext(ctx, token, id)
	if err != nil {
		return fmt.Errorf("failed to UpdateTokenIdById: %w", err)
	}
	return nil
}

const verifyUserByToken = `
UPDATE "users" SET
is_verified = true,
token_verification = NULL, token_expiration = NULL
WHERE id = $1
`

func (us *UserStore) VerifyUserByToken(ctx context.Context, id int) error {
	_, err := us.ps.VerifyUserByToken.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("failed to VerifyUserByToken: %w", err)
	}
	return nil
}

const userUpdateTokenByEmail = `
UPDATE "users" SET
token_verification = $1, token_expiration = $2
WHERE email = $3
`

func (us *UserStore) UpdateTokenByEmail(ctx context.Context, usr *store.UserRegister) error {
	_, err := us.ps.UpdateTokenByEmail.ExecContext(ctx,
		usr.TokenVerification, usr.TokenExpiration, usr.Email,
	)
	if err != nil {
		return fmt.Errorf("failed to UpdateTokenByEmail: %w", err)
	}
	return nil
}

const userDeleteTokenById = `
UPDATE "users" SET
token_id = NULL
WHERE id = $1
`

func (us *UserStore) DeleteTokenId(ctx context.Context, id int) error {
	_, err := us.ps.DeleteTokenById.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("failed to DeleteTokenById: %w", err)
	}
	return nil
}

func (us *UserStore) scanRow(row *sql.Row) (*store.User, error) {
	user := &store.User{}
	err := row.Scan(
		&user.ID, &user.Email, &user.Fullname,
		&user.IsVerified, &user.TokenID, &user.TokenVerification,
		&user.TokenExpiration,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to scanRow: %w", err)
	}
	return user, nil
}

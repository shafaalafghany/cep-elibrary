package postgresql

import (
	"cep-elibrary/store"
	"context"
	"database/sql"
	"fmt"
)

type BookStore struct {
	db *sql.DB
	ps *bookPrepareStatement
}

func NewBookStore(db *sql.DB) (*BookStore, error) {
	bs := &BookStore{
		db: db,
		ps: &bookPrepareStatement{},
	}
	err := bs.prepareStatement()
	if err != nil {
		return nil, err
	}
	return bs, nil
}

func (bs *BookStore) prepareStatement() error {
	storeName := "BookStore"
	var err error
	if bs.ps.FindByAuthorAndTitle, err = prepareStatement(
		bs.db,
		storeName,
		"FindOneByAuthorAndTitle",
		bookFindByAuthorAndTitle,
	); err != nil {
		return err
	}
	if bs.ps.FindNewBooks, err = prepareStatement(
		bs.db,
		storeName,
		"findNewBooks",
		findNewBooks,
	); err != nil {
		return err
	}
	if bs.ps.FindPopularBooks, err = prepareStatement(
		bs.db,
		storeName,
		"FindPopularBooks",
		findPopularBooks,
	); err != nil {
		return err
	}
	// if bs.ps.FindWithFilter, err = prepareStatement(
	// 	bs.db,
	// 	storeName,
	// 	"FindWithFilter",

	// )
	return nil
}

type bookPrepareStatement struct {
	FindByAuthorAndTitle *sql.Stmt
	FindWithFilter       *sql.Stmt
	FindNewBooks         *sql.Stmt
	FindPopularBooks     *sql.Stmt
}

const bookFindBase = `
SELECT id, title, author,
synopsis, cover, category_id
FROM "books"
`

const bookFindByAuthorAndTitle = `
SELECT id, title, author,
synopsis, cover, reader,
ROUND(rating::numeric, 2) rating
FROM "books"
WHERE author ILIKE '%' || $1 || '%' OR 
title ILIKE '%' || $2 || '%'
ORDER BY title
`

func (bs *BookStore) FindByAuthorAndTitle(ctx context.Context, kwAuthor, kwTitle string) ([]*store.Book, error) {
	var rating sql.NullFloat64
	row, err := bs.ps.FindByAuthorAndTitle.QueryContext(ctx, kwAuthor, kwTitle)
	if err != nil {
		return nil, fmt.Errorf("failed to query %w", err)
	}
	list := make([]*store.Book, 0)
	defer func() { _ = row.Close() }()
	for row.Next() {
		book := &store.Book{}
		err := row.Scan(
			&book.ID, &book.Title, &book.Author, &book.Synopsis,
			&book.Cover, &book.Reader, &rating,
		)
		if err != nil {
			return nil, fmt.Errorf("failed to scanRow %w", err)
		}
		if !rating.Valid {
			book.Rating = 0
		} else {
			book.Rating = rating.Float64
		}
		list = append(list, book)
	}
	return list, nil
}

const bookFindWithFilter = bookFindBase + `
WHERE id
`

const findNewBooks = `
SELECT id, title, author, cover
FROM "books"
ORDER BY title
`

func (bs *BookStore) FindNewBooks(ctx context.Context) ([]*store.Book, error) {
	row, err := bs.ps.FindNewBooks.QueryContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to query %w", err)
	}
	defer func() { _ = row.Close() }()
	list := make([]*store.Book, 0)
	for row.Next() {
		book := &store.Book{}
		err := row.Scan(
			&book.ID, &book.Title, &book.Author,
			&book.Cover,
		)
		if err != nil {
			return nil, fmt.Errorf("failed to scanRow %w", err)
		}
		list = append(list, book)
	}
	return list, nil
}

const findPopularBooks = `
SELECT id, title, author, cover, reader,
ROUND(rating::numeric, 2) rating
FROM "books"
ORDER BY rating DESC, reader DESC
`

func (bs *BookStore) FindPopularBooks(ctx context.Context) ([]*store.Book, error) {
	var rating sql.NullFloat64
	row, err := bs.ps.FindPopularBooks.QueryContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to query")
	}
	defer func() { _ = row.Close() }()
	list := make([]*store.Book, 0)
	for row.Next() {
		book := &store.Book{}
		err := row.Scan(
			&book.ID, &book.Title, &book.Author,
			&book.Cover, &book.Reader, &rating,
		)
		if err != nil {
			return nil, fmt.Errorf("failed to scanRow")
		}
		if !rating.Valid {
			book.Rating = 0
		} else {
			book.Rating = rating.Float64
		}
		list = append(list, book)
	}
	return list, nil
}

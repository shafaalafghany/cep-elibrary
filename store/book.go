package store

import (
	"context"
)

type Book struct {
	ID       int     `json:"id"`
	Title    string  `json:"title"`
	Author   string  `json:"author"`
	Synopsis string  `json:"synopsis"`
	Cover    string  `json:"cover"`
	Reader   int     `json:"reader"`
	Rating   float64 `json:"rating"`
}

type BookStore interface {
	FindByAuthorAndTitle(ctx context.Context, kwAuthor, kwTitle string) ([]*Book, error)
	FindNewBooks(ctx context.Context) ([]*Book, error)
	FindPopularBooks(ctx context.Context) ([]*Book, error)
}

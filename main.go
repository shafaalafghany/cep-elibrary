package main

import (
	"cep-elibrary/api"
	"cep-elibrary/config"
	"cep-elibrary/googleauth"
	"cep-elibrary/logger"
	mailer "cep-elibrary/mail"
	"cep-elibrary/postgresql"
	"cep-elibrary/token"
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/rs/zerolog"
)

func main() {
	cfg, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal(err)
		return
	}
	var zlog zerolog.Logger
	logCfg := logger.Config{
		Level:  cfg.LoggerLevel,
		Output: cfg.LoggerOutput,
	}
	zlog, err = logger.NewZerolog(logCfg)
	if err != nil {
		log.Fatal(fmt.Errorf("failed to create zerolog console: %w", err))
	}
	ctx := context.Background()
	jwt := setupJWT(cfg)
	elibraryDB := pg(cfg, zlog)
	db := api.DB{
		ELibraryPostgres: elibraryDB,
	}
	mail := setupMail(cfg)
	googleAuthValidator, err := setupGoogleOauth(ctx, cfg)
	if err != nil {
		log.Fatal(err)
	}

	apiLogger := zlog.With().
		Str("component", "api").
		Logger()

	tokenVerification := api.TokenVerificationConfig{
		Expiry: time.Duration(cfg.TokenVerificationExpiration),
	}

	srv := api.NewServer(
		fmt.Sprintf("%s:%d", cfg.AppHost, cfg.AppPort),
		apiLogger,
		db,
		jwt,
		googleAuthValidator,
		mail,
		tokenVerification,
	)
	srv.Run(ctx)
}

func pg(cfg config.Config, logger zerolog.Logger) *sql.DB {
	db, err := postgresql.Open(cfg)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to open postgres db")
		return nil
	}

	err = db.Ping()
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to ping postgres db")
		return nil
	}
	return db
}

func setupJWT(cfg config.Config) token.JWT {
	jwtCfg := token.JWTConfig{
		TokenAccessExpiration:  time.Duration(cfg.TokenAccessExpiration),
		TokenRefreshExpiration: time.Duration(cfg.TokenRefreshExpiration),
	}

	return token.NewJWT(jwtCfg)
}

func setupGoogleOauth(
	ctx context.Context,
	cfg config.Config,
) (googleauth.Validator, error) {
	c := googleauth.Config{
		OauthAudience: cfg.GoogleOauthAudience,
	}
	return googleauth.NewValidator(ctx, c)
}

func setupMail(
	cfg config.Config,
) mailer.EmailSender {
	c := &mailer.Config{
		AppUrl:       fmt.Sprintf("%s://%s:%d", cfg.AppProtocol, cfg.AppHost, cfg.AppPort),
		MailHost:     cfg.MailHost,
		MailPort:     cfg.MailPort,
		MailUsername: cfg.MailUsername,
		MailPassword: cfg.MailPassword,
	}
	return mailer.NewMail(c)
}

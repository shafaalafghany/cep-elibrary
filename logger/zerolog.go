package logger

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
)

type Config struct {
	Level  string
	Output string
}

func NewZerolog(cfg Config) (zerolog.Logger, error) {
	writer := zerolog.ConsoleWriter{Out: os.Stdout}
	logLvl, err := zerolog.ParseLevel(cfg.Level)
	if err != nil {
		return zerolog.Nop(), fmt.Errorf("invalid rezolog log level: %w", err)
	}
	zlog := zerolog.New(writer).
		Level(logLvl).
		With().
		Timestamp().
		Logger()

	return zlog, nil
}

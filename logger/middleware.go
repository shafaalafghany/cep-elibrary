package logger

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
)

type reqIdCtxKey struct{}

const ReqIdKey = "requestId"

func HTTPRreqId(zlog zerolog.Logger) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			ctx := r.Context()
			id := uuid.NewString()
			ctx = context.WithValue(ctx, reqIdCtxKey{}, id)
			r = r.WithContext(ctx)
			h.ServeHTTP(w, r)
			end := time.Since(start).Milliseconds()
			zlog.Info().
				Str("path", r.URL.Path).
				Str("query", r.URL.RawQuery).
				Int64("duration", end).
				Msg(fmt.Sprintf("%s %s", r.Method, r.URL.Path))
		})
	}
}

func ReqID(ctx context.Context) string {
	if id, ok := ctx.Value(reqIdCtxKey{}).(string); ok {
		return id
	}
	return ""
}

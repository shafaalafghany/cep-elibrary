package token

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	apierror "cep-elibrary/api/error"
	"cep-elibrary/api/response"
)

type tokenCtxKey struct{}

func Validate(jwt JWT) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			header := strings.TrimSpace(r.Header.Get("Authorization"))
			if header == "" {
				response.Error(w, apierror.ClientUnauthorized())
				return
			}
			tokenStr, err := getTokenFromHeader(header)
			if err != nil {
				response.Error(w, apierror.ClientUnauthorized())
				return
			}
			claim, err := jwt.ExpectAccessToken(tokenStr)
			if err != nil {
				if err == JWTExpirationError {
					response.Error(w, apierror.ClientAccessExpired())
					return
				}
				response.Error(w, apierror.ClientUnauthorized())
				return
			}
			ctx := r.Context()
			ctx = context.WithValue(ctx, tokenCtxKey{}, claim)
			r = r.WithContext(ctx)
			h.ServeHTTP(w, r)
		})
	}
}

func getTokenFromHeader(token string) (string, error) {
	header := strings.Split(token, " ")
	if len(header) != 2 {
		return "", fmt.Errorf("auth header is not Bearer scheme")
	}
	if header[0] != bearerScheme {
		return "", fmt.Errorf("auth header is not Bearer scheme")
	}
	return strings.TrimSpace(header[1]), nil
}

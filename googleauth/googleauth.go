package googleauth

import (
	"context"
	"fmt"
	"time"

	"google.golang.org/api/idtoken"
	"google.golang.org/api/option"
)

type Config struct {
	OauthAudience string
}

type Validator interface {
	Validate(ctx context.Context, token string) (Claim, error)
}

type GoogleValidator struct {
	cfg       Config
	validator *idtoken.Validator
}

type Claim struct {
	Email      string
	Fullname   string
	IsVerified bool
}

const googleIssuer = "https://accounts.google.com"

func NewValidator(ctx context.Context, cfg Config) (Validator, error) {
	idToken, err := idtoken.NewValidator(ctx, option.WithoutAuthentication())
	if err != nil {
		return nil, fmt.Errorf("failed to create new google auth validator")
	}
	v := &GoogleValidator{
		validator: idToken,
	}
	return v, nil
}

func (v *GoogleValidator) Validate(ctx context.Context, token string) (Claim, error) {
	p := Claim{}

	idToken, err := v.validator.Validate(ctx, token, v.cfg.OauthAudience)
	if err != nil {
		return p, fmt.Errorf("failed to validate token: %w", err)
	}
	if idToken.Issuer != googleIssuer {
		return p, fmt.Errorf("token is not from google account")
	}
	if idToken.Expires < time.Now().Unix() {
		return p, fmt.Errorf("token is expired")
	}

	claim := idToken.Claims
	p.Email, _ = claim["email"].(string)
	p.Fullname, _ = claim["name"].(string)
	p.IsVerified, _ = claim["email_verified"].(bool)

	return p, nil
}

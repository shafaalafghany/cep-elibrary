# E-Library Backend

---

Welcome to e-library backend 🎉🎉🎉

# Technology Stack

---

1. Golang backend programming
2. PostgreSql (database)

# Database Schema

---

Below is how the database schema looks like
![elibrary](erd.jpg)
